const forceFailingFunctionByPercent = (func, percent) => (...props) => {
  if (Math.random() >= 1 - (percent / 100)) {
    throw new Error('Unluko maluko!')
  }

  return func(...props)
}

module.exports = forceFailingFunctionByPercent
