const { forceFailingFunctionByPercent } = require('../utils')
const { isString } = require('../validators')

const unstableIsString = forceFailingFunctionByPercent(isString, 10)

describe('utils', () => {
  // Падает 1 раз из 10
  it.skip('Happy path | correct', () => {
    expect(unstableIsString('1')).toBeTruthy()
  })
})
