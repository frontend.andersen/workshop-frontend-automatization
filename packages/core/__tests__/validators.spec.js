const validators = require('../validators')

describe('validators', () => {
  describe('isString', () => {
    it('Happy path | correct', () => {
      expect(validators.isString('1')).toBeTruthy()
    })

    it('Happy path | incorrect', () => {
      expect(validators.isString(1)).toBeFalsy()
    })
  })

  describe('isFunction', () => {
    it('Happy path | correct', () => {
      expect(validators.isFunction(() => {})).toBeTruthy()
    })

    it('Happy path | incorrect', () => {
      expect(validators.isFunction({})).toBeFalsy()
    })
  })
})
