const isString = (target) => typeof target === 'string'

const isFunction = (target) => typeof target === 'function'

module.exports = {
  isString,
  isFunction
}
